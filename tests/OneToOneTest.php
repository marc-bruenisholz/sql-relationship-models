<?php
/*
 * Copyright 2020 Marc Brünisholz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

namespace ch\_4thewin\SqlRelationshipModels;

use ch\_4thewin\SqlSelectModels\Table;
use PHPUnit\Framework\TestCase;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertTrue;

class OneToOneTest extends TestCase
{
    public function test()
    {
        $oneToOne = new OneToOne(
            new Table('owningTableName', 'id','string','owningTableAlias'),
            'foreignKeyColumnName','string',
            new Table('inverseTableName', 'primaryKeyColumnName','string','inverseTableAlias'),
            true
        );

        assertEquals('owningTableName', $oneToOne->getOwningTable()->getName());
        assertEquals('owningTableAlias', $oneToOne->getOwningTable()->getAlias());
        assertEquals('foreignKeyColumnName', $oneToOne->getForeignKeyColumnName());
        assertEquals('inverseTableName', $oneToOne->getInverseTable()->getName());
        assertEquals('inverseTableAlias', $oneToOne->getInverseTable()->getAlias());
        assertEquals('primaryKeyColumnName', $oneToOne->getInverseTable()->getPrimaryKeyColumnName());
        self::assertTrue($oneToOne->isFromInverseToOwningTable());

        $oneToOne->setIsFromInverseToOwningTable(false);
        self::assertFalse($oneToOne->isFromInverseToOwningTable());
        assertEquals(new Table('inverseTableName', 'primaryKeyColumnName','string','inverseTableAlias'), $oneToOne->getTargetTable());

        assertTrue($oneToOne instanceof ToOne);
    }

}
