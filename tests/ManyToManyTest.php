<?php
/*
 * Copyright 2020 Marc Brünisholz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

namespace ch\_4thewin\SqlRelationshipModels;

use ch\_4thewin\SqlSelectModels\ParameterizedSqlInterface;
use ch\_4thewin\SqlSelectModels\Table;
use PHPUnit\Framework\TestCase;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertTrue;

class ManyToManyTest extends TestCase
{
    public function test()
    {
        $manyToMany = new ManyToMany(
            new Table('firstTableName', 'firstTablePrimaryKeyColumnName','string','firstTableAlias'),
            new Table('intermediaryTableName', 'id','string','intermediaryTableAlias'),
            'firstForeignKeyColumnName', 'string',
            'secondForeignKeyColumnName', 'string',
            new Table('secondTableName', 'secondTablePrimaryKeyColumnName','string','secondTableAlias')
        );

        assertEquals('firstTableName', $manyToMany->getOneToMany()->getInverseTable()->getName());
        assertEquals('firstTableAlias', $manyToMany->getOneToMany()->getInverseTable()->getAlias());
        assertEquals('firstTablePrimaryKeyColumnName', $manyToMany->getOneToMany()->getInverseTable()->getPrimaryKeyColumnName());
        assertEquals('intermediaryTableName', $manyToMany->getOneToMany()->getOwningTable()->getName());
        assertEquals('intermediaryTableName', $manyToMany->getManyToOne()->getOwningTable()->getName());
        assertEquals('intermediaryTableAlias', $manyToMany->getOneToMany()->getOwningTable()->getAlias());
        assertEquals('intermediaryTableAlias', $manyToMany->getManyToOne()->getOwningTable()->getAlias());
        assertEquals('firstForeignKeyColumnName', $manyToMany->getOneToMany()->getForeignKeyColumnName());
        assertEquals('secondForeignKeyColumnName', $manyToMany->getManyToOne()->getForeignKeyColumnName());
        assertEquals('secondTableName', $manyToMany->getManyToOne()->getInverseTable()->getName());
        assertEquals('secondTableAlias', $manyToMany->getManyToOne()->getInverseTable()->getAlias());
        assertEquals('secondTablePrimaryKeyColumnName', $manyToMany->getManyToOne()->getInverseTable()->getPrimaryKeyColumnName());


        $inSearchCondition = new class implements ParameterizedSqlInterface {

            function getArguments(): array
            {
                return [];
            }

            function toString(): string
            {
                return '';
            }
        };
        $manyToMany->getOneToMany()->setSearchCondition($inSearchCondition);
        assertEquals($inSearchCondition, $manyToMany->getOneToMany()->getSearchCondition());

        assertTrue($manyToMany instanceof ToMany);
    }

}
