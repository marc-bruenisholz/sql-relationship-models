<?php
/*
 * Copyright 2020 Marc Brünisholz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

namespace ch\_4thewin\SqlRelationshipModels;

use ch\_4thewin\SqlSelectModels\Table;
use PHPUnit\Framework\TestCase;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertFalse;
use function PHPUnit\Framework\assertTrue;

class ManyToOneTest extends TestCase
{
    public function test()
    {
        $manyToOne = new ManyToOne(
            new Table('owningTableName', 'id','string','owningTableAlias'),
            'foreignKeyColumnName','string',
            new Table('inverseTableName', 'primaryKeyColumnName','string','inverseTableAlias')
        );

        assertEquals('owningTableName', $manyToOne->getOwningTable()->getName());
        assertEquals('owningTableAlias', $manyToOne->getOwningTable()->getAlias());
        assertEquals('foreignKeyColumnName', $manyToOne->getForeignKeyColumnName());
        assertEquals('inverseTableName', $manyToOne->getInverseTable()->getName());
        assertEquals('inverseTableAlias', $manyToOne->getInverseTable()->getAlias());
        assertEquals('primaryKeyColumnName', $manyToOne->getInverseTable()->getPrimaryKeyColumnName());
        assertFalse($manyToOne->isFromInverseToOwningTable());
        assertEquals(new Table('inverseTableName', 'primaryKeyColumnName','string','inverseTableAlias'), $manyToOne->getTargetTable());

        assertTrue($manyToOne instanceof ToOne);
    }

}
