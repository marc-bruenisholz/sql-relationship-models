<?php

namespace ch\_4thewin\SqlRelationshipModels;

use ch\_4thewin\SqlSelectModels\StringInterface;

class PropertySortOrder
{
    protected StringInterface $columnExpression;

    /** @var string One of ASC or DESC */
    protected string $order;

    /**
     * @param StringInterface $columnExpression
     * @param string $order
     */
    public function __construct(StringInterface $columnExpression, string $order)
    {
        $this->columnExpression = $columnExpression;
        $this->order = $order;
    }

    /**
     * @return StringInterface
     */
    public function getColumnExpression(): StringInterface
    {
        return $this->columnExpression;
    }


    /**
     * @return string
     */
    public function getOrder(): string
    {
        return $this->order;
    }



}