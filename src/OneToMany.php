<?php
/*
 * Copyright 2020 Marc Brünisholz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

namespace ch\_4thewin\SqlRelationshipModels;


use ch\_4thewin\SqlSelectModels\Page;
use ch\_4thewin\SqlSelectModels\StringInterface;
use ch\_4thewin\SqlSelectModels\Table;

/**
 * The OneToMany relationship is achieved
 * by a foreign key column on the second table.
 * It contains primary keys of the first table.
 * @package ch\_4thewin\SqlRelationshipSelectQueryBuilder\Models
 */
class OneToMany  extends SingleCardinalityRelationship implements ToMany
{
    /** @var Page|null */
    protected ?Page $page = null;

    /**
     * @var AggregateFunction[]
     */
    protected array $aggregateFunctions = [];

    /** @var PropertySortOrder[] */
    protected array $propertySortOrders = [];

    /**
     * SingleCardinalityRelationship constructor.
     * @param Table $owningTable
     * @param string $foreignKeyColumnName
     * @param string $foreignKeyColumnType
     * @param Table $inverseTable
     * @param Page|null $page
     */
    public function __construct(
        Table $owningTable,
        string $foreignKeyColumnName,
        string $foreignKeyColumnType,
        Table $inverseTable,
        ?Page $page = null
    ) {
        parent::__construct($owningTable, $foreignKeyColumnName, $foreignKeyColumnType, $inverseTable);
        $this->page = $page;
        $this->owningTable = $owningTable;
        $this->foreignKeyColumnName = $foreignKeyColumnName;
        $this->inverseTable = $inverseTable;
    }

    public function isFromInverseToOwningTable(): bool
    {
        return true;
    }

    /**
     * Overrides method from ToMany interface
     * @return Page|null
     */
    public function getPage(): ?Page
    {
        return $this->page;
    }

    /**
     * @param Page|null $page
     * @return self
     */
    public function setPage(?Page $page): self
    {
        $this->page = $page;
        return $this;
    }

    /**
     * @return AggregateFunction[]
     */
    public function getAggregateFunctions(): array
    {
        return $this->aggregateFunctions;
    }

    /**
     * @param AggregateFunction[] $aggregateFunctions
     * @return self
     */
    public function setAggregateFunctions(array $aggregateFunctions): self
    {
        $this->aggregateFunctions = $aggregateFunctions;
        return $this;
    }


    public function getTargetTable(): Table
    {
        return $this->getOwningTable();
    }

    public function getSourceTable(): Table
    {
        return $this->getInverseTable();
    }

    public function addPropertySortOrder(StringInterface $columnExpression, string $order): ToMany
    {
        $this->propertySortOrders[] = new PropertySortOrder($columnExpression, $order);
        return $this;
    }

    public function getPropertySortOrders(): array
    {
        return $this->propertySortOrders;
    }
}