<?php
/*
 * Copyright 2020 Marc Brünisholz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

namespace ch\_4thewin\SqlRelationshipModels;


use ch\_4thewin\SqlSelectModels\Page;
use ch\_4thewin\SqlSelectModels\StringInterface;
use ch\_4thewin\SqlSelectModels\Table;

/**
 * A ManyToMany relationship consists of two relationships. The first table has
 * a OneToMany relationship with the intermediary table that holds
 * the foreign key columns. The intermediary table has
 * a ManyToOne relation to the second table.
 * @package ch\_4thewin\SqlRelationshipSelectQueryBuilder\Models
 */
class ManyToMany extends Relationship implements ToMany
{
    /**
     * The relationship from the first table
     * to the intermediary table.
     * @var OneToMany
     */
    protected OneToMany $oneToMany;

    /**
     * The relationship from the intermediary
     * table to the second table.
     * @var ManyToOne
     */
    protected ManyToOne $manyToOne;

    /**
     * @param Table $firstTable
     * @param Table $intermediaryTable
     * @param string $firstForeignKeyColumnName
     * @param string $firstForeignKeyColumnType
     * @param string $secondForeignKeyColumnName
     * @param string $secondForeignKeyColumnType
     * @param Table $secondTable
     * @param Page|null $page
     */
    public function __construct(
        Table $firstTable,
        Table $intermediaryTable,
        string $firstForeignKeyColumnName,
        string $firstForeignKeyColumnType,
        string $secondForeignKeyColumnName,
        string $secondForeignKeyColumnType,
        Table $secondTable,
        ?Page $page = null)
    {
        $this->oneToMany = new OneToMany($intermediaryTable, $firstForeignKeyColumnName, $firstForeignKeyColumnType, $firstTable, $page);
        $this->manyToOne = new ManyToOne($intermediaryTable, $secondForeignKeyColumnName, $secondForeignKeyColumnType, $secondTable);
    }

    /**
     * @return OneToMany
     */
    public function getOneToMany(): OneToMany
    {
        return $this->oneToMany;
    }

    /**
     * @return ManyToOne
     */
    public function getManyToOne(): ManyToOne
    {
        return $this->manyToOne;
    }

    /**
     * Overrides method from ToMany interface
     * @return Page|null
     */
    public function getPage(): ?Page
    {
        return $this->oneToMany->getPage();
    }

    /**
     * Overrides method from ToMany interface
     * @param Page|null $page
     * @return self
     */
    public function setPage(?Page $page): self
    {
        $this->oneToMany->setPage($page);
        return $this;
    }

    public function getAggregateFunctions(): array
    {
        return $this->oneToMany->getAggregateFunctions();

    }

    public function setAggregateFunctions(array $aggregateFunctions): self
    {
        $this->oneToMany->setAggregateFunctions($aggregateFunctions);
        return $this;
    }


    public function getTargetTable(): Table
    {
        return $this->manyToOne->getInverseTable();
    }

    public function getSourceTable(): Table
    {
        return $this->oneToMany->getInverseTable();
    }

    public function addPropertySortOrder(StringInterface $columnExpression, string $order): ToMany
    {
        $this->oneToMany->addPropertySortOrder($columnExpression, $order);
        return $this;
    }

    public function getPropertySortOrders(): array
    {
        return $this->oneToMany->getPropertySortOrders();
    }
}