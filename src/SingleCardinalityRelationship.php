<?php
/*
 * Copyright 2020 Marc Brünisholz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

namespace ch\_4thewin\SqlRelationshipModels;


use ch\_4thewin\SqlSelectModels\ParameterizedSqlInterface;
use ch\_4thewin\SqlSelectModels\Table;

/**
 * A relationship with a single cardinality on either
 * side is achieved by a foreign key column on the
 * "many" side. The table with the foreign key column
 * is called "owning table". The other table is called
 * "inverse table". The inverse table holds the
 * primary keys that are used in the foreign key column.
 * @package ch\_4thewin\SqlRelationshipSelectQueryBuilder\Models
 */
abstract class SingleCardinalityRelationship extends Relationship
{
    /**
     * This is the table holding the foreign key column.
     * @var Table
     */
    protected Table $owningTable;

    /**
     * The name of the foreign key column
     * that contains primary keys of another
     * table.
     * @var string
     */
    protected string $foreignKeyColumnName;

    /**
     * The type of the foreign key value.
     * @var string One of 'integer' or 'string'
     */
    protected string $foreignKeyColumnType;


    /**
     * The table of which the primary
     * keys are used in the
     * foreign key column of the
     * table owning the relationship.
     * @var Table
     */
    protected Table $inverseTable;

    /**
     * This search condition is added
     * to the ID comparison join condition
     * in the sql ON clause with logical AND.
     * The join type is changed to INNER JOIN
     * if a search condition is added.
     * @var ParameterizedSqlInterface|null
     */
    protected ?ParameterizedSqlInterface $searchCondition = null;

    protected ?ParameterizedSqlInterface $accessControlCondition = null;
    
    /** @var string|null */
    protected ?string $joinType = null;

    /**
     * SingleCardinalityRelationship constructor.
     * @param Table $owningTable
     * @param string $foreignKeyColumnName
     * @param string $foreignKeyColumnType
     * @param Table $inverseTable
     */
    public function __construct(
        Table $owningTable,
        string $foreignKeyColumnName,
        string $foreignKeyColumnType,
        Table $inverseTable
    ) {
        $this->owningTable = $owningTable;
        $this->foreignKeyColumnName = $foreignKeyColumnName;
        $this->foreignKeyColumnType = $foreignKeyColumnType;
        $this->inverseTable = $inverseTable;
    }

    /**
     * Is true if the foreign key column
     * is on the second table of this
     * relationship or false if it
     * is on the first table.
     * @var bool
     * @return bool
     */
    abstract public function isFromInverseToOwningTable(): bool;

    /**
     * @return Table
     */
    public function getOwningTable(): Table
    {
        return $this->owningTable;
    }

    /**
     * @return string
     */
    public function getForeignKeyColumnName(): string
    {
        return $this->foreignKeyColumnName;
    }

    /**
     * @return string
     */
    public function getForeignKeyColumnType(): string
    {
        return $this->foreignKeyColumnType;
    }

    /**
     * @return Table
     */
    public function getInverseTable(): Table
    {
        return $this->inverseTable;
    }

    /**
     * @return ParameterizedSqlInterface|null
     */
    public function getSearchCondition(): ?ParameterizedSqlInterface
    {
        return $this->searchCondition;
    }

    public function setSearchCondition(?ParameterizedSqlInterface $searchCondition): self
    {
        $this->searchCondition = $searchCondition;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getJoinType(): ?string
    {
        return $this->joinType;
    }

    /**
     * @param string|null $joinType
     * @return SingleCardinalityRelationship
     */
    public function setJoinType(?string $joinType): self
    {
        $this->joinType = $joinType;
        return $this;
    }

    /**
     * @return ParameterizedSqlInterface|null
     */
    public function getAccessControlCondition(): ?ParameterizedSqlInterface
    {
        return $this->accessControlCondition;
    }

    /**
     * @param ParameterizedSqlInterface|null $accessControlCondition
     */
    public function setAccessControlCondition(?ParameterizedSqlInterface $accessControlCondition): void
    {
        $this->accessControlCondition = $accessControlCondition;
    }
    
    

}