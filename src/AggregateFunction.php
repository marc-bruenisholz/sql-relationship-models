<?php

namespace ch\_4thewin\SqlRelationshipModels;

// TODO does this class belong here?
class AggregateFunction
{
    protected string $functionName;
    protected string $propertyName;

    /**
     * @param string $functionName
     * @param string $propertyName
     */
    public function __construct(string $functionName, string $propertyName)
    {
        $this->functionName = $functionName;
        $this->propertyName = $propertyName;
    }

    /**
     * @return string
     */
    public function getFunctionName(): string
    {
        return $this->functionName;
    }

    /**
     * @return string
     */
    public function getPropertyName(): string
    {
        return $this->propertyName;
    }

    

}