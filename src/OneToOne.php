<?php
/*
 * Copyright 2020 Marc Brünisholz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

namespace ch\_4thewin\SqlRelationshipModels;


use ch\_4thewin\SqlSelectModels\Table;

/**
 * The OneToOne relationship is achieved
 * by a foreign key column. The foreign key column
 * can be on either table. To signify whether
 * the first or the second
 * table holds the foreign key column, the
 * boolean $isFromInverseToOwningTable has to be set.
 * If true, the foreign key column is on the second table.
 * @package ch\_4thewin\SqlRelationshipSelectQueryBuilder\Models
 */
class OneToOne extends SingleCardinalityRelationship implements ToOne
{
    /** @var bool */
    protected bool $isFromInverseToOwningTable;

    /**
     * SingleCardinalityRelationship constructor.
     * @param Table $owningTable
     * @param string $foreignKeyColumnName
     * @param string $foreignKeyColumnType
     * @param Table $inverseTable
     * @param bool $isFromInverseToOwningTable
     */
    public function __construct(
        Table $owningTable,
        string $foreignKeyColumnName,
        string $foreignKeyColumnType,
        Table $inverseTable,
        bool $isFromInverseToOwningTable
    ) {
        parent::__construct($owningTable, $foreignKeyColumnName, $foreignKeyColumnType, $inverseTable);
        $this->isFromInverseToOwningTable = $isFromInverseToOwningTable;
    }

    /**
     * @return bool
     */
    public function isFromInverseToOwningTable(): bool
    {
        return $this->isFromInverseToOwningTable;
    }

    public function setIsFromInverseToOwningTable(bool $isFromInverseToOwningTable): self
    {
        $this->isFromInverseToOwningTable = $isFromInverseToOwningTable;
        return $this;
    }

    public function getTargetTable(): Table
    {
        return $this->isFromInverseToOwningTable ? $this->getOwningTable() : $this->getInverseTable();
    }

    public function getSourceTable(): Table
    {
        return $this->isFromInverseToOwningTable ? $this->getInverseTable() : $this->getOwningTable();
    }
}